# Grafana Panel Plugin Template

[![Build](https://github.com/grafana/grafana-starter-panel/workflows/CI/badge.svg)](https://github.com/grafana/grafana-starter-panel/actions?query=workflow%3A%22CI%22)

This template is a starting point for building Grafana Panel Plugins in Grafana 7.0+

## What is Grafana Panel Plugin?

Panels are the building blocks of Grafana. They allow you to visualize data in different ways. While Grafana has several types of panels already built-in, you can also build your own panel, to add support for other visualizations.

For more information about panels, refer to the documentation on [Panels](https://grafana.com/docs/grafana/latest/features/panels/panels/)

## Getting started

1. Install dependencies

   ```bash
   yarn install
   ```

2. Build plugin in development mode or run in watch mode

   ```bash
   yarn dev
   ```

   or

   ```bash
   yarn watch
   ```

3. Build plugin in production mode

   ```bash
   yarn build
   ```

## Learn more

- [Build a panel plugin tutorial](https://grafana.com/tutorials/build-a-panel-plugin)
- [Grafana documentation](https://grafana.com/docs/)
- [Grafana Tutorials](https://grafana.com/tutorials/) - Grafana Tutorials are step-by-step guides that help you make the most of Grafana
- [Grafana UI Library](https://developers.grafana.com/ui) - UI components to help you build interfaces using Grafana Design System





className = {lineas_sistema1.l1_1}


 //PSG2A, TDLOWA ,ATS03A,TDP01A
    l1_1: string;//path6103-7
    l1_2: string;//path6183
    l1_3: string;//path6185
    l1_4: string;//path11202
    l1_5: string;//path11200
    l1_6: string;//path6673
    l1_7: string;//path15921
    l1_8: string;//path15966

    //TDP01A,ATSREC08A,ATSANTSAT1A,ATSREC05A,TRDRY,TUPSIN01A,ATSCHI08A,ATSSG11A,ATSCHI07A
    l1_9: string;//path5903
    l1_10: string;//path5901
    l1_11: string;//path5899
    l1_12: string;//path5894
    l1_13: string;//path5892
    l1_14: string;//path5890
    l1_15: string;//path5886
    l1_16: string;//path20299
    l1_17: string;//path6683
    //TR-DRY; P-UPS2A
    l1_18: string;//path6603-8
    l1_19: string;//path6605-3
    l1_20: string;//path6609-1
    l1_21: string;//path6607-0

    //ATSANTSAT1A,CUARTO SATELITAL
    l1_22: string;//path16574
    l1_23: string;//path16572

    //ATSREC05A, REC01A
    l1_24: string;//path20297
    
    //TUPSIN01A1 
    l1_25: string;//path16768
    l1_26: string;//path16770
    l1_27: string;//path16772
    l1_28: string;//path16774
    l1_29: string;//path16776
    l1_30: string;//path16778

    // TUPSOUT01A
    l1_31: string;//path16780
    l1_32: string;//path16782
    l1_33: string;//path16784
    l1_34: string;//path16786
    l1_35: string;//path16788
    l1_36: string;//path16790

    //TUPSOUT01A,ATS-PDU-10A
    l1_37: string;//path20323

    //TUPSOUT01A, ATS-PDU-09A
    l1_38: string;//path6304

    //ATS-PDU-09A, T-PDU-01A
    l1_39: string;//path6519

    //ATS-CHI-07A, T-CHI-03A
    l1_40: string;//path5544

    //T-CHI-03A, T-CHI-01A
    l1_41: string;//path9357

    //ATS-SG-11A, TSG-1A
    l1_42: string;//path6542
    l1_43: string;//path6544    